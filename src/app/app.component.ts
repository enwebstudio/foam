import { Component } from '@angular/core';
import { SidebarService } from '../service/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ SidebarService ]
})
export class AppComponent {

  constructor(
    public sidebarService: SidebarService
  ) {
  }

}
