import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AccountHeaderComponent } from './account-header/account-header.component';
import { AccountIndexComponent } from './account-index/account-index.component';
import { AccountSidebarComponent } from './account-sidebar/account-sidebar.component';
import { AccountOrdersComponent } from './account-orders/account-orders.component';
import { AccountOrderComponent } from './account-order/account-order.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountHeaderComponent,
    AccountIndexComponent,
    AccountSidebarComponent,
    AccountOrdersComponent,
    AccountOrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
