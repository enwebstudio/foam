import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-sidebar',
  templateUrl: './account-sidebar.component.html',
  styleUrls: ['./account-sidebar.component.scss']
})
export class AccountSidebarComponent implements OnInit {

  mainNavItemList = [
    {
      title: 'Главная',
      url: '/index',
      iconName: 'icon-page-main'
    },
    {
      title: 'Заявки',
      url: '/orders',
      iconName: 'icon-page-requests'
    },
    {
      title: 'Автомойки',
      url: '#',
      iconName: 'icon-page-car-wash'
    },
    {
      title: 'Сотрудники',
      url: '#',
      iconName: 'icon-page-employees'
    },
    {
      title: 'Услуги',
      url: '#',
      iconName: 'icon-page-services'
    },
    {
      title: 'Скидки',
      url: '#',
      iconName: 'icon-page-discounts'
    },
    {
      title: 'Статистика',
      url: '#',
      iconName: 'icon-page-statistics'
    },
    {
      title: 'Настройки',
      url: '#',
      iconName: 'icon-page-settings'
    },
    {
      title: 'Обратная связь',
      url: '#',
      iconName: 'icon-page-feedback'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
