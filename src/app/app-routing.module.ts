import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountIndexComponent } from './account-index/account-index.component';
import { AccountOrdersComponent } from './account-orders/account-orders.component';
import { AccountOrderComponent } from './account-order/account-order.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
  {
    path: 'index',
    component: AccountIndexComponent
  },
  {
    path: 'orders',
    component: AccountOrdersComponent
  },
  {
    path: 'order',
    component: AccountOrderComponent
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes)],
  providers: [],
  bootstrap: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
