import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../service/sidebar.service';

@Component({
  selector: 'app-account-header',
  templateUrl: './account-header.component.html',
  styleUrls: ['./account-header.component.scss']
})
export class AccountHeaderComponent implements OnInit {

  constructor(public sidebarService: SidebarService) { }

  ngOnInit() { }

  openSidebar() {
    if (this.sidebarService.isSidebarOpened) this.sidebarService.isSidebarOpened = false;
    else this.sidebarService.isSidebarOpened = true;
  }

}
