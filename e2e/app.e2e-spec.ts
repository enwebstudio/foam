import { FoamAppPage } from './app.po';

describe('foam-app App', function() {
  let page: FoamAppPage;

  beforeEach(() => {
    page = new FoamAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
